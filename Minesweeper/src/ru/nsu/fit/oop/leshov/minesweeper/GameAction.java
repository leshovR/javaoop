package ru.nsu.fit.oop.leshov.minesweeper;

public enum GameAction {
    OPEN,
    FLAG,
    RESTART
}
