package ru.nsu.fit.oop.leshov.minesweeper;


public class GameActionEvent implements Event {
    private final GameAction action;
    private final int[] args;


    public GameActionEvent(GameAction type, int... args) {
        action = type;
        this.args = args;
    }

    public GameAction getType() {
        return action;
    }

    public int[] getArgs() {
        return args;
    }
}
