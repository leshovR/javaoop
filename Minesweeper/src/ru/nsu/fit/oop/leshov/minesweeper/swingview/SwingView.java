package ru.nsu.fit.oop.leshov.minesweeper.swingview;

import ru.nsu.fit.oop.leshov.minesweeper.*;
import ru.nsu.fit.oop.leshov.minesweeper.game.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

public class SwingView extends AbstractPublisher implements View {
    private final Window window;
    private final int cellSize;

    private final World world;
    private boolean done = false;
    private Info info;
    private Timer timer;

    public SwingView() {
        cellSize = 20;
        Map<String, Image> cellImages = new HashMap<>();
        setImages(cellImages);
        world = new World(cellImages);
        info = new Info();

        window = new Window(world.getFieldSize().x , world.getFieldSize().y);
        window.getPanel().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int x = e.getX() - world.getLocation().x;
                int y = e.getY() - world.getLocation().y;

                if (x < 0 || y < 0) return;

                int col = x / cellSize;
                int row = y / cellSize;
                if (e.getButton() == MouseEvent.BUTTON1) {
                    notifySubscribers(new GameActionEvent(GameAction.OPEN, col, row));
                } else if (e.getButton() == MouseEvent.BUTTON3) {
                    notifySubscribers(new GameActionEvent(GameAction.FLAG, col, row));
                }
                window.repaint();
            }
        });

        timer = new Timer(1, e -> notifySubscribers(new RenderEvent(null)));
        timer.start();

        window.setIconImage(getImage("icon"));
        window.addGameComponent(info);
        window.addGameComponent(world);
        setupMenu();
    }

    private void setupMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Game");

        JMenuItem restart = new JMenuItem("Restart");
        restart.addActionListener(e -> notifySubscribers(new GameActionEvent(GameAction.RESTART, 0, 0, 0)));
        menu.add(restart);

        JMenuItem easy = new JMenuItem("Easy");
        easy.addActionListener(e -> notifySubscribers(new GameActionEvent(GameAction.RESTART, 8, 8, 10)));
        menu.add(easy);

        JMenuItem medium = new JMenuItem("Medium");
        medium.addActionListener(e -> notifySubscribers(new GameActionEvent(GameAction.RESTART, 16, 16, 40)));
        menu.add(medium);

        JMenuItem hard = new JMenuItem("Hard");
        hard.addActionListener(e -> notifySubscribers(new GameActionEvent(GameAction.RESTART, 16, 31, 99)));
        menu.add(hard);

        menuBar.add(menu);
        window.setJMenuBar(menuBar);
        window.pack();
    }

    private Image getImage(String name) {
        String filename = "img/" + name + ".png";
        ImageIcon icon = new ImageIcon(ClassLoader.getSystemResource(filename));
        return icon.getImage();
    }

    private void setImages(Map<String, Image> cellImages) {
        for (Cell cell : Cell.values()) {
            String name = cell.name().toLowerCase();
            cellImages.put(name, getImage(name));
        }

        for (CellState state : CellState.values()) {
            String name = state.name().toLowerCase();
            cellImages.put(name, getImage(name));
        }
    }

    @Override
    public void display(Model game) {
        info.update(game);
        world.update(game);
        window.pack();
        window.repaint();
    }

    @Override
    public void finish(Model game) {
        if (done) return;
        done = true;
        game.finish();
        display(game);
        timer.restart();
    }

    @Override
    public void update(RenderEvent event) {
        if (event.getModel().isDone()) finish(event.getModel());
        else done = false;

        if (!window.isVisible()) {
            window.setVisible(true);
        }

        world.update(event.getModel());
        display(event.getModel());
    }
}
