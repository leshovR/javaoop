package ru.nsu.fit.oop.leshov.minesweeper.swingview;

import ru.nsu.fit.oop.leshov.minesweeper.game.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Info extends JPanel {
    private JLabel timeLabel;
    private JLabel mineLabel;
    private Timer timer;

    public Info() {
        setBackground(Color.lightGray);
        setLayout(new BorderLayout(10, 0));

        timeLabel = new JLabel("00:00");
        Font font = new Font("Arial", Font.PLAIN, 14);
        timeLabel.setFont(font);


        mineLabel = new JLabel("10");
        mineLabel.setFont(font);

        add(mineLabel, BorderLayout.WEST);
        add(timeLabel, BorderLayout.EAST);
    }

    public void update(Model game) {
        long ms = game.getElapsedTime();
        timeLabel.setText(ms / (1000 * 60) + ":" + ms / 1000 % 60);
        mineLabel.setText(Integer.toString(game.getMinesRemaining()));
    }
}
