package ru.nsu.fit.oop.leshov.minesweeper.game;

public enum CellState {
    OPEN,
    CLOSE,
    FLAG,
    QUESTION
}
