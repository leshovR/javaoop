package ru.nsu.fit.oop.leshov.minesweeper;

public interface Subscriber<T extends Event> {
    void update(T event);
}
