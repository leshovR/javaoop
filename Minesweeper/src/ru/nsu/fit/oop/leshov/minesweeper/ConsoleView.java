package ru.nsu.fit.oop.leshov.minesweeper;

import ru.nsu.fit.oop.leshov.minesweeper.game.Cell;
import ru.nsu.fit.oop.leshov.minesweeper.game.CellState;
import ru.nsu.fit.oop.leshov.minesweeper.game.GameMatrix;
import ru.nsu.fit.oop.leshov.minesweeper.game.Model;

import java.util.*;

public class ConsoleView extends AbstractPublisher implements View {
    private final Map<Cell, Character> symbols;
    private final Map<String, GameAction> actions;
    private final Scanner scanner;
    private boolean done;

    public ConsoleView() {
        done = false;
        scanner = new Scanner(System.in);
        symbols = new HashMap<>();

        for (Cell cell : Cell.values()) {
            symbols.put(cell, (char) (cell.number() + '0'));
        }
        symbols.put(Cell.MINE, '*');
        symbols.put(Cell.EMPTY, ' ');


        actions = new HashMap<>();
        actions.put("open", GameAction.OPEN);
        actions.put("flag", GameAction.FLAG);
        actions.put("restart", GameAction.RESTART);
    }

    @Override
    public void display(Model game) {
        System.out.print("     ");
        GameMatrix field = game.getField();

        for (int i = 0; i < field.width(); i++) {
            System.out.printf("%-3d", i);
        }
        System.out.println();
        System.out.print("    ");
        for (int i = 0; i < field.width(); i++) {
            System.out.print("---");
        }
        System.out.println();

        for (int y = 0; y < field.height(); y++) {
            System.out.print(y + "| ");
            for (int x = 0; x < field.width(); x++) {
                if (field.getCellState(x, y) == CellState.QUESTION) {
                    System.out.printf("%3c", '?');
                } else if (field.getCellState(x, y) == CellState.FLAG) {
                    System.out.printf("%3c", 'F');
                } else if (field.getCellState(x, y) == CellState.CLOSE) {
                    System.out.printf("%3c", '-');
                } else {
                    System.out.printf("%3c", symbols.get(field.getCell(x, y)));
                }
            }

            System.out.println();
        }
    }


    private void readInput() {
        String[] words = scanner.nextLine().split(" +");
        String actionName = words[0];
        ArrayList<Integer> args = new ArrayList<>();

        for (int i = 1; i < words.length; ++i) {
            args.add(Integer.parseInt(words[i]));
        }

        GameAction action = actions.getOrDefault(actionName, null);

        notifySubscribers(new GameActionEvent(action,
                args.stream().mapToInt(i -> i).toArray()));
    }

    @Override
    public void finish(Model game) {
        if (done) return;

        done = true;
        System.out.println("Game over");
        long seconds = game.getElapsedTime() / 1000;
        System.out.println("Time: " + seconds / 60 + ":" + seconds % 60);
    }

    @Override
    public void update(RenderEvent event) {
        display(event.getModel());

        if (!event.getModel().isDone()) {
            readInput();
        } else {
            finish(event.getModel());
        }
    }
}
