package ru.nsu.fit.oop.leshov.minesweeper;

import ru.nsu.fit.oop.leshov.minesweeper.game.Model;

public interface View extends Publisher, Subscriber<RenderEvent> {
    void display(Model model);
    void finish(Model model);
}
