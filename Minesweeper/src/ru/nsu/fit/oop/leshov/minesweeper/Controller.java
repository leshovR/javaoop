package ru.nsu.fit.oop.leshov.minesweeper;

import ru.nsu.fit.oop.leshov.minesweeper.game.Model;

public class Controller implements Subscriber<Event> {
    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
        this.view.subscribe(this);
        model.subscribe(view);
    }

    @Override
    public void update(Event event) {
        if (event == null) return;

        if (event instanceof GameActionEvent)
            model.update((GameActionEvent) event);
        else
            view.update(new RenderEvent(model));
    }

    public void start() {
        view.update(new RenderEvent(model));
    }
}
