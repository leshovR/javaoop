package ru.nsu.fit.oop.leshov.minesweeper;

import ru.nsu.fit.oop.leshov.minesweeper.game.Model;
import ru.nsu.fit.oop.leshov.minesweeper.swingview.SwingView;

public class Main {

    public static void main(String[] args) {
        Model model = new Model(8, 8, 10);
        View view = new ConsoleView();
        Controller game = new Controller(model, view);
        game.start();
    }
}
