package ru.nsu.fit.oop.leshov.minesweeper.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ranges {
    private static Point size;
    private static final Random random = new Random();

    public static Point getSize() {
        return size;
    }

    public static void setSize(Point size) {
        Ranges.size = size;
    }

    public static int width() {
        return size.x;
    }

    public static int height() {
        return size.y;
    }

    public static int square() {
        return size.x * size.y;
    }


    public static boolean inRange(Point point) {
        return point.x >= 0 && point.x < size.x &&
                point.y >= 0 && point.y < size.y;
    }

    public static Point getRandomCoord() {
        return new Point(random.nextInt(size.x), random.nextInt(size.y));
    }

    public static List<Point> getCoordsAround(Point point) {
        ArrayList<Point> list = new ArrayList<>();
        for (int y = point.y - 1; y <= point.y + 1; ++y) {
            for (int x = point.x - 1; x <= point.x + 1; ++x) {
                Point current = new Point(x, y);
                if (inRange(current) && !current.equals(point)) {
                    list.add(current);
                }
            }
        }

        return list;
    }

}
