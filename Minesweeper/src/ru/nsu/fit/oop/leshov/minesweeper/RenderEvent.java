package ru.nsu.fit.oop.leshov.minesweeper;


import ru.nsu.fit.oop.leshov.minesweeper.game.Model;

public class RenderEvent implements Event {
    private final Model game;

    public RenderEvent(Model game) {
        this.game = game;
    }

    public Model getModel() {
        return game;
    }
}
