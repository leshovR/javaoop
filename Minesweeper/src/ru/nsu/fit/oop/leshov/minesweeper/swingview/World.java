package ru.nsu.fit.oop.leshov.minesweeper.swingview;

import ru.nsu.fit.oop.leshov.minesweeper.game.*;
import ru.nsu.fit.oop.leshov.minesweeper.game.Point;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

class World extends JComponent {
    private Point fieldSize;

    private final int cellSize;
    private final Map<String, Image> cellImages;
    private GameMatrix field;


    public World(Map<String, Image> images) {
        field = new GameMatrix(Ranges.getSize());

        cellImages = images;
        cellSize = 20;
        adjustSize();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int y = 0; y < Ranges.height(); y++) {
            for (int x = 0; x < Ranges.width(); x++) {
                CellState cell = field.getCellState(x, y);
                String imgName = cell.name().toLowerCase();
                if (cell == CellState.OPEN) {
                    imgName = field.getCell(x, y).name().toLowerCase();
                }

                g.drawImage(cellImages.get(imgName),
                        x * cellSize, y * cellSize, this);
            }
        }
    }

    public void update(Model game) {
        field = game.getField();
        adjustSize();
    }

    private void adjustSize() {
        fieldSize = new Point(Ranges.width()*cellSize,
                Ranges.height()*cellSize);
        setPreferredSize(new Dimension(fieldSize.x,
                fieldSize.y));
        setMinimumSize(new Dimension(fieldSize.x, fieldSize.y));
        setMaximumSize(new Dimension(fieldSize.x, fieldSize.y));
        revalidate();
    }


    public Point getFieldSize() {
        return fieldSize;
    }
}
