package ru.nsu.fit.oop.leshov.minesweeper.game;

import ru.nsu.fit.oop.leshov.minesweeper.*;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;


public class Model extends AbstractPublisher
        implements Subscriber<GameActionEvent> {
    private int cols;
    private int rows;
    private int mines;

    private boolean won = false;

    private GameMatrix field;
    private final Map<GameAction, Consumer<int[]>> reactions;

    private boolean done = false;

    private long startTime;
    private long currentTime;


    public boolean isDone() {
        return done;
    }


    public Model(int cols, int rows, int minesCount) {
        initialize(new int[]{cols, rows, minesCount});
        reactions = new HashMap<>();
        reactions.put(GameAction.OPEN, this::open);
        reactions.put(GameAction.FLAG, this::flag);
        reactions.put(GameAction.RESTART, this::initialize);
    }

    private void initialize(int[] args) {
        if (args.length != 3) {
            throw new IllegalArgumentException("initialize takes 3 arguments");
        }
        rows = args[1] == 0 ? rows : args[0];
        cols = args[0] == 0 ? cols : args[1];
        mines = args[2] == 0 ? mines : args[2];

        startTime = currentTime = 0;
        done = false;
        won = false;

        Ranges.setSize(new Point(cols, rows));

        field = new GameMatrix(Ranges.height(), Ranges.width());

        for (int y = 0; y < Ranges.height(); y++) {
            for (int x = 0; x < Ranges.width(); x++) {
                field.setCell(x, y, Cell.EMPTY);
                field.setCellState(x, y, CellState.CLOSE);
            }
        }
        notifySubscribers(new RenderEvent(this));
    }


    public void update(GameActionEvent event) {
        Consumer<int[]> reaction =
                reactions.getOrDefault(event.getType(), null);
        if (reaction != null) {
            reaction.accept(event.getArgs());
        }

        if (field.getClosedCells() == mines) {
            done = true;
            won = true;
        }
        notifySubscribers(new RenderEvent(this));
    }

    public long getElapsedTime() {
        if (startTime != 0 && (!done || currentTime == 0)) {
            currentTime = System.currentTimeMillis();
        }

        return currentTime - startTime;
    }


    public GameMatrix getField() {
        return field;
    }


    private void landMines(int minesCount) {
        int mine = 0;
        while (mine != minesCount) {
            Point minePosition = Ranges.getRandomCoord();
            if (field.getCell(minePosition) != Cell.MINE &&
                    field.getCellState(minePosition) != CellState.OPEN) {
                ++mine;
                land(minePosition);
            }
        }
    }


    private void land(Point mine) {
        field.setCell(mine, Cell.MINE);

        for (Point point : Ranges.getCoordsAround(mine)) {
            if (field.getCell(point).number() >= 0)
                field.setCell(point, field.getCell(point).next());
        }
    }


    // Opens a cell
    private void open(int[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException("Open takes 2 arguments");
        }
        Point point = new Point(args[0], args[1]);
        if (done) return;

        if (field.getClosedCells() == rows * cols) {
            startTime = System.currentTimeMillis();
            field.setCellState(point, CellState.OPEN);
            landMines(mines);
            field.setCellState(point, CellState.CLOSE);
        }

        done = field.openCell(point);


        if (field.getCell(point) == Cell.MINE) {
            done = true;
            won = false;
        }
    }


    private void flag(int[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException("Open takes 2 arguments");
        }
        if (done) return;

        field.flagCell(new Point(args[0], args[1]));
    }


    public void finish() {
        if (won) {
            for (int y = 0; y < Ranges.height(); ++y) {
                for (int x = 0; x < Ranges.width(); ++x) {
                    if (field.getCellState(x, y) != CellState.OPEN) {
                        field.setCellState(x, y, CellState.FLAG);
                    }
                }
            }
        } else {
            for (int y = 0; y < Ranges.height(); ++y) {
                for (int x = 0; x < Ranges.width(); ++x) {
                    Cell cell = field.getCell(x, y);
                    CellState cellState = field.getCellState(x, y);
                    switch (cellState) {
                        case OPEN:
                            if (cell == Cell.MINE) {
                                field.setCell(x, y, Cell.EXPLOSION);
                            }
                            break;

                        case QUESTION:
                        case CLOSE:
                            if (cell == Cell.MINE) {
                                field.setCellState(x, y, CellState.OPEN);
                            }
                            break;

                        case FLAG:
                            if (cell != Cell.MINE) {
                                field.setCell(x, y, Cell.WRONGFLAG);
                                field.setCellState(x, y, CellState.OPEN);
                            }
                            break;
                    }
                }
            }
        }
    }

    public int getMinesRemaining() {
        return Math.max(mines - field.getFlagsCount(), 0);
    }
}
