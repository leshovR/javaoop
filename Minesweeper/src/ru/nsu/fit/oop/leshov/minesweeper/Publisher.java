package ru.nsu.fit.oop.leshov.minesweeper;

interface Publisher {
    void subscribe(Subscriber<? extends Event> subscriber);
}
