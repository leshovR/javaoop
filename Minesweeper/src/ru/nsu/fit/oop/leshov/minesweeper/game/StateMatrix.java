package ru.nsu.fit.oop.leshov.minesweeper.game;

public class StateMatrix {
    private final CellState[][] matrix;
    private int closedCells;

    public StateMatrix(int rows, int cols) {
        matrix = new CellState[rows][cols];
        for (int y = 0; y < matrix.length; y++) {
            for (int x = 0; x < matrix[y].length; x++) {
                matrix[y][x] = CellState.CLOSE;
            }
        }

        closedCells = rows * cols;
    }

    public CellState getCell(int x, int y) {
        return matrix[y][x];
    }

    public CellState getCell(Point point) {
        return getCell(point.x, point.y);
    }

    public void setCell(int x, int y, CellState value) {
        if (value == CellState.OPEN && matrix[y][x] != CellState.OPEN) {
            closedCells -= 1;
        } else if (value == CellState.CLOSE && matrix[y][x] == CellState.OPEN) {
            closedCells += 1;
        }
        matrix[y][x] = value;
    }

    public void setCell(Point point, CellState value) {
        setCell(point.x, point.y, value);
    }

    public int getClosedCells() {
        return closedCells;
    }
}
