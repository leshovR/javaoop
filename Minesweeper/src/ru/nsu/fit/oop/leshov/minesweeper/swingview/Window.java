package ru.nsu.fit.oop.leshov.minesweeper.swingview;

import ru.nsu.fit.oop.leshov.minesweeper.game.Point;

import javax.swing.*;
import java.awt.*;

class Window extends JFrame {
    private final Point size;
    private JPanel panel;

    public Window(Point size) {
        this.size = size;
        initPanel();
        initFrame();
    }

    private void initPanel() {
        panel = new JPanel();
        panel.setMinimumSize(new Dimension(size.x, size.y));
        panel.setMaximumSize(Toolkit.getDefaultToolkit().getScreenSize());
        add(panel);
    }

    public Window(int x, int y) {
        this(new Point(x, y));
    }

    private void initFrame() {
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        //panel.setLayout(new BorderLayout(1, 1));
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Minesweeper");
        setLocationRelativeTo(null);
        setResizable(false);
    }


    public void addGameComponent(JComponent component) {
       // setResizable(true);
        panel.add(component);
        pack();
        setLocationRelativeTo(null);
        // setResizable(false);
        size.x = getWidth();
        size.y = getHeight();
    }

    public JPanel getPanel() {
        return panel;
    }
}
