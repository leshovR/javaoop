package ru.nsu.fit.oop.leshov.minesweeper.game;

public class GameMatrix {
    private final Cell[][] matrix;
    private final StateMatrix fieldState;
    private int flagsCount;

    public GameMatrix(int rows, int cols) {
        matrix = new Cell[rows][cols];
        fieldState = new StateMatrix(rows, cols);
        flagsCount = 0;
    }

    public GameMatrix(Point size) {
        this(size.y, size.x);
    }

    public Cell getCell(int x, int y) {
        return matrix[y][x];
    }

    public Cell getCell(Point point) {
        return getCell(point.x, point.y);
    }

    public void setCell(int x, int y, Cell value) {
        matrix[y][x] = value;
    }

    public void setCell(Point point, Cell value) {
        setCell(point.x, point.y, value);
    }

    public CellState getCellState(int x, int y) {
        return fieldState.getCell(x, y);
    }

    public CellState getCellState(Point point) {
        return fieldState.getCell(point);
    }

    public void setCellState(int x, int y, CellState state) {
        fieldState.setCell(x, y, state);
    }

    public void setCellState(Point point, CellState state) {
        fieldState.setCell(point, state);
    }

    public int width() {
        return matrix[0].length;
    }

    public int height() {
        return matrix.length;
    }


    public void flagCell(Point point) {
        if (getCellState(point) == CellState.OPEN) return;

        if (getCellState(point) == CellState.CLOSE) {
            flagsCount += 1;
            setCellState(point, CellState.FLAG);
        } else if (getCellState(point) == CellState.FLAG) {
            flagsCount -= 1;
            setCellState(point, CellState.QUESTION);
        } else {
            setCellState(point, CellState.CLOSE);
        }
    }


    public boolean openCell(Point point) {
        boolean ret = false;
        if (getCellState(point) == CellState.FLAG ||
                getCellState(point) == CellState.QUESTION) return false; // Don't open if flagged

        if (getCell(point) == Cell.MINE) ret = true;

        if (getCellState(point) == CellState.OPEN) {
            return checkAdjacent(point);
        }

        setCellState(point, CellState.OPEN);

        if (getCell(point) == Cell.EMPTY) {
            return openAdjacent(point);
        }
        return ret;
    }


    // Acts if the cell is already open
    private boolean checkAdjacent(Point point) {
        if (getCell(point).number() <= 0) return false;

        // Check the flags count around the cell
        int count = 0;
        for (Point adj : Ranges.getCoordsAround(point)) {
            if (getCellState(adj) == CellState.FLAG) {
                ++count;
            }
        }

        if (count == getCell(point).number()) {
            return openAdjacent(point);
        }
        return false;
    }


    private boolean openAdjacent(Point point) {
        boolean ret = false;
        for (Point adj : Ranges.getCoordsAround(point)) {
            if (getCellState(adj) == CellState.CLOSE) {
                ret = openCell(adj);
            }
        }
        return ret;
    }


    public int getClosedCells() {
        return fieldState.getClosedCells();
    }

    public int getFlagsCount() {
        return flagsCount;
    }
}
