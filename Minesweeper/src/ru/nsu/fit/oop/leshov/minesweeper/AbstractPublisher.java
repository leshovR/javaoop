package ru.nsu.fit.oop.leshov.minesweeper;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractPublisher implements Publisher {

    private final List<Subscriber<?>> subscribers = new ArrayList<>();

    @Override
    public void subscribe(Subscriber<? extends Event> subscriber) {
        subscribers.add(subscriber);
    }

    @SuppressWarnings("unchecked")
    protected void notifySubscribers(Event event) {
        for (Subscriber s : subscribers) {
            s.update(event);
        }
    }
}
