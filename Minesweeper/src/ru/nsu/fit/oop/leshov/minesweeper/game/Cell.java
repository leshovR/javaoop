package ru.nsu.fit.oop.leshov.minesweeper.game;

public enum Cell {
    EMPTY,
    NUM1,
    NUM2,
    NUM3,
    NUM4,
    NUM5,
    NUM6,
    NUM7,
    NUM8,
    MINE,
    EXPLOSION,
    WRONGFLAG;

    public Cell next() {
        return Cell.values()[ordinal() + 1];
    }

    public int number() {
        if (name().startsWith("NUM")) {
            return ordinal() - NUM1.ordinal() + 1;
        } else if (name().equals("EMPTY")) {
            return 0;
        }
        return -1;
    }
}
