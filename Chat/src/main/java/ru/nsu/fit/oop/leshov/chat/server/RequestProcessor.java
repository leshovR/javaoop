package ru.nsu.fit.oop.leshov.chat.server;

import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;

class RequestProcessor implements Runnable {
    private final static Logger LOG = Logger.getLogger(RequestProcessor.class.getName());
    private Socket s;
    RequestProcessor(Socket client) {
        s = client;
    }

    @Override
    public void run() {
        try {
            InputStream input = s.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            OutputStream output = s.getOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(output);

            String line;
            while (true) {
                LOG.info("\n\nWaiting for the string");
                line = reader.readLine();
                if (line == null) {
                    break;
                }

                System.out.println("Received the string: " + line);
                writer.write("Answer from: " + s.getLocalAddress() + ":" +
                        s.getLocalPort() + " to: " + s.getInetAddress() + ":" +
                        s.getPort() + ": " + line + "\n");

                writer.flush();
            }

            writer.close();
            LOG.info("Request processing finished\n\n");
        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }
}
