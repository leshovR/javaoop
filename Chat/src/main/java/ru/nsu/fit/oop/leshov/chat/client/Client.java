package ru.nsu.fit.oop.leshov.chat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        try (Socket s = new Socket("127.0.0.1", 2048)) {
            OutputStreamWriter writer = new OutputStreamWriter(s.getOutputStream());
            BufferedReader consoleReader =
                    new BufferedReader(new InputStreamReader(System.in));

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(s.getInputStream()));

            String line;
            do {
                System.out.println("Please enter a string");
                line = consoleReader.readLine();
                if (line.equalsIgnoreCase("exit")) break;
                writer.write(line + "\n");
                writer.flush();
                line = reader.readLine();
                System.out.println("Received answer: " + line);
            } while (true);

            writer.close();
        } catch(IOException ex) {
            System.err.println(ex.getLocalizedMessage());
        }
    }
}
