package ru.nsu.fit.oop.leshov.chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class Server {
    private final static Logger LOG = Logger.getLogger(Server.class.getName());

    public static void main(String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(2048);
        LOG.info("Server launched");

        while(true) {
            Socket client = socket.accept();
            LOG.info("Received message from " + client.getInetAddress() +
                    ":" + client.getPort());

            Thread t = new Thread(new RequestProcessor(client));
            LOG.info("Starting request processor");
            t.start();
        }
    }
}
