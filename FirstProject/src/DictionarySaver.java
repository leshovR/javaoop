import java.io.*;
import java.util.*;

class DictionarySaver implements TableSaver {
    private Writer writer;

    DictionarySaver(Writer wr) {
        writer = wr;
    }

    public void write(Iterator<List> content) throws IOException {
        while(content.hasNext()) {
            List entry = content.next();

            Iterator itr = entry.iterator();
            while (itr.hasNext()) {
                Object obj = itr.next();
                writer.append(obj.toString());
                if (itr.hasNext())
                    writer.append(", ");
            }
            writer.append("\n");
        }
    }
}
