import java.io.IOException;
import java.util.Iterator;
import java.util.List;


public interface TableSaver {
    void write(Iterator<List> content) throws IOException;
}
