import java.io.*;

class WordReader implements Tokenizer {
    private Reader reader;

    WordReader(Reader r) {
        reader = r;
    }


    public String next() {
        int c;
        StringBuilder word = new StringBuilder();

        try {
            while ((c = reader.read()) > -1) {
                char ch = (char) c;
                if (Character.isLetterOrDigit(ch)) {
                    word.append(ch);
                } else if (word.length() > 0) {
                    break;
                }
            }
        }
        catch(IOException e) {
            throw new RuntimeException(e);
        }
        if (word.length() > 0) {
            return word.toString();
        }
        return null;
    }
}
