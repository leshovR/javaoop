import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DictionaryAggregator implements Aggregator {
    private Map<String, Integer> dictionary = new HashMap<>();
    private int total = 0;

    public void addToken(String token) {
        Integer frequency = dictionary.getOrDefault(token, 0);
        dictionary.put(token, frequency + 1);
        ++total;
    }

    public List<List> finish() {
        List<List> retList = new ArrayList<>();
        for (Map.Entry<String, Integer> e : dictionary.entrySet()) {
            List<Object> list = new ArrayList<>();
            list.add(e.getKey());
            list.add(e.getValue());
            list.add(e.getValue() * 100.f / total);
            retList.add(list);
        }

        return retList;
    }
}
