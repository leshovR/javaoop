import java.io.*;
import java.util.*;

public class Main
{
    public static void main(String[] args)
    {
        if (args.length != 2) {
            if (args.length < 2) {
                System.out.println("Not enough arguments");
            } else {
                System.out.println("Too many arguments");
            }
            return;
        }
        DictionaryAggregator aggregator = new DictionaryAggregator();
        try(Reader reader = new InputStreamReader(new BufferedInputStream(new FileInputStream(args[0])))) {
            WordReader wordReader = new WordReader(reader);
            String word;
            while ((word = wordReader.next()) != null) {
                aggregator.addToken(word);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return;
        }
        List<List> table = aggregator.finish();
        table.sort((o1, o2) -> {
            int rs = Integer.compare((int)o2.get(1), (int)o1.get(1));
            if (rs == 0) {
                rs = ((String)o1.get(0)).compareTo((String)o2.get(0));
            }
            return rs;
        });
        try(Writer writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(args[1])))) {
            DictionarySaver saver = new DictionarySaver(writer);
            saver.write(table.iterator());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
