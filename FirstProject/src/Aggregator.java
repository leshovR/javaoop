import java.util.List;

public interface Aggregator {
    void addToken(String token);

    List<List> finish();
}
