
public interface Tokenizer {
    String next();
}
