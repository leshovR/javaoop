package ru.nsu.java.instructions.controls;

import org.junit.jupiter.api.Test;
import ru.nsu.java.exceptions.InstructionArgumentException;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.instructions.InstructionTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PushTest extends InstructionTest {
    private Push command;

    PushTest() throws IOException {
        super();
        command = new Push();
    }

    @Test
    void pushPositive() {
        args.add("4.2");
        assertDoesNotThrow(() -> command.execute(context, args));
        assertEquals(1, context.stackSize());
        assertEquals(4.2, context.popFromStack());

        args.clear();
        context.addDefinition("name", 15.21);
        args.add("name");
        assertDoesNotThrow(() -> command.execute(context, args));
        assertEquals(1, context.stackSize());
        assertEquals(15.21, context.popFromStack());
    }

    @Test
    void pushNegative() {
        Exception e = assertThrows(InstructionException.class,
                () -> command.execute(null, args));
        assertEquals("No context", e.getMessage());

        e = assertThrows(InstructionArgumentException.class,
                () -> command.execute(context, args));
        assertEquals("Not enough arguments", e.getMessage());


        args.add("notExistingName");
        e = assertThrows(InstructionArgumentException.class,
                () -> command.execute(context, args));
        assertEquals("Bad argument", e.getMessage());
    }
}