package ru.nsu.java.instructions.controls;

import org.junit.jupiter.api.Test;
import ru.nsu.java.exceptions.InstructionArgumentException;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.instructions.InstructionTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class DefineTest extends InstructionTest {
    private Define command;

    DefineTest() throws IOException {
        super();
        command = new Define();
    }

    @Test
    void definePositive() {
        args.add("a");
        args.add("4.2");

        assertDoesNotThrow(() -> command.execute(context, args));
        assertEquals(4.2, context.getDefinition("a"));
    }

    @Test
    void defineNegative() {
        Exception e = assertThrows(InstructionException.class,
                () -> command.execute(null, args));
        assertEquals("No context", e.getMessage());

        e = assertThrows(InstructionArgumentException.class,
                () -> command.execute(context, args));
        assertEquals("Wrong arguments", e.getMessage());

        args.add("Name");
        args.add("value");
        e = assertThrows(InstructionArgumentException.class,
                () -> command.execute(context, args));
        assertEquals("value is not a valid argument", e.getMessage());

        args.clear();
        args.add("!34D naM3");
        args.add("42.42");
        e = assertThrows(InstructionArgumentException.class,
                () -> command.execute(context, args));
        assertEquals("!34D naM3 is not a valid name", e.getMessage());

    }
}