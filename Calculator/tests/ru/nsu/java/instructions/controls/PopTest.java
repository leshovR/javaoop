package ru.nsu.java.instructions.controls;

import org.junit.jupiter.api.Test;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.exceptions.StackException;
import ru.nsu.java.instructions.InstructionTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PopTest extends InstructionTest {
    private Pop command;

    PopTest() throws IOException {
        super();
        command = new Pop();
    }

    @Test
    void popPositive() {
        context.pushToStack(4.2);
        context.pushToStack(15.21);

        assertDoesNotThrow(() -> command.execute(context, args));
        assertEquals(1, context.stackSize());
        assertEquals(4.2, context.popFromStack());

        args.add("name");
        context.pushToStack(4.2);
        assertDoesNotThrow(() -> command.execute(context, args));
        assertEquals(4.2, context.getDefinition("name"));
    }

    @Test
    void popNegative() {
        Exception e = assertThrows(InstructionException.class,
                () -> command.execute(null, args));
        assertEquals("No context", e.getMessage());

        e = assertThrows(StackException.class,
                () -> command.execute(context, args));
        assertEquals("Not enough values in the stack", e.getMessage());


        args.add("name1");
        args.add("name2");
        context.pushToStack(4.2);
        e = assertThrows(StackException.class,
                () -> command.execute(context, args));
        assertEquals("Can't pop this much", e.getMessage());
    }
}