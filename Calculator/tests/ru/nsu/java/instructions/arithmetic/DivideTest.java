package ru.nsu.java.instructions.arithmetic;

import org.junit.jupiter.api.Test;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.exceptions.StackException;
import ru.nsu.java.instructions.InstructionTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class DivideTest extends InstructionTest {
    private Divide command;

    DivideTest() throws IOException {
        super();
        command = new Divide();
    }


    @Test
    void dividePositive() {
        context.pushToStack(8.4);
        context.pushToStack(4.2);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(2.0, context.popFromStack());

        context.pushToStack(-4.2);
        context.pushToStack(4.2);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(-1.0, context.popFromStack());

        context.pushToStack(-4.2);
        context.pushToStack(-4.2);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(1.0, context.popFromStack());

        context.pushToStack(4.2);
        context.pushToStack(0.0);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(Double.POSITIVE_INFINITY, context.popFromStack());

        context.pushToStack(-4.2);
        context.pushToStack(0.0);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(Double.NEGATIVE_INFINITY, context.popFromStack());

        context.pushToStack(0.0);
        context.pushToStack(0.0);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(Double.NaN, context.popFromStack());

        context.pushToStack(5.0);
        context.pushToStack(2.0);
        assertDoesNotThrow(() -> command.execute(context,
                Collections.emptyList()));
    }

    @Test
    void divideNegative() {
        Exception e = assertThrows(StackException.class, () -> command.execute(context, null));
        assertEquals("Not enough arguments in stack", e.getMessage());

        context.pushToStack(-4.2);
        e = assertThrows(StackException.class, () -> command.execute(context, null));
        assertEquals("Not enough arguments in stack", e.getMessage());

        e = assertThrows(InstructionException.class, () -> command.execute(context,
                Arrays.asList(String.valueOf(4.2), String.valueOf(4.2))));
        assertEquals("Wrong usage of DIVIDE", e.getMessage());

        e = assertThrows(InstructionException.class, () -> command.execute(null, null));
        assertEquals("No context", e.getMessage());
    }
}