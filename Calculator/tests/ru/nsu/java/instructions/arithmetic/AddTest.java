package ru.nsu.java.instructions.arithmetic;

import org.junit.jupiter.api.Test;

import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.exceptions.StackException;
import ru.nsu.java.instructions.InstructionTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class AddTest extends InstructionTest {
    private Add command;

    AddTest() throws IOException {
        super();
        command = new Add();
    }

    @Test
    void addPositive() {
        context.pushToStack(4.2);
        context.pushToStack(4.2);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(8.4, context.popFromStack());

        context.pushToStack(-4.2);
        context.pushToStack(4.2);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(0.0, context.popFromStack());

        context.pushToStack(-4.2);
        context.pushToStack(-4.2);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(-8.4, context.popFromStack());

        context.pushToStack(4.2);
        context.pushToStack(4.2);
        assertDoesNotThrow(() -> command.execute(context,
                Collections.emptyList()));
    }

    @Test
    void addNegative() {
        Exception e = assertThrows(StackException.class, () -> command.execute(context, null));
        assertEquals("Not enough arguments in stack", e.getMessage());

        context.pushToStack(-4.2);
        e = assertThrows(StackException.class, () -> command.execute(context, null));
        assertEquals("Not enough arguments in stack", e.getMessage());

        e = assertThrows(InstructionException.class, () -> command.execute(context,
                Arrays.asList(String.valueOf(4.2), String.valueOf(4.2))));
        assertEquals("Wrong usage of ADD", e.getMessage());

        e = assertThrows(InstructionException.class, () -> command.execute(null, null));
        assertEquals("No context", e.getMessage());
    }
}