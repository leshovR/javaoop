package ru.nsu.java.instructions.arithmetic;

import org.junit.jupiter.api.Test;
import ru.nsu.java.exceptions.InstructionArgumentException;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.exceptions.StackException;
import ru.nsu.java.instructions.InstructionTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class SquareRootTest extends InstructionTest {
    private SquareRoot command;

    SquareRootTest() throws IOException {
        super();
        command = new SquareRoot();
    }


    @Test
    void squareRootPositive() {
        context.pushToStack(2.25);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(1.5, context.popFromStack());

        context.pushToStack(0.0);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(0.0, context.popFromStack());

        context.pushToStack(Double.POSITIVE_INFINITY);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(Double.POSITIVE_INFINITY, context.popFromStack());

        context.pushToStack(Double.NaN);
        assertDoesNotThrow(() -> command.execute(context, null));
        assertEquals(Double.NaN, context.popFromStack());

        context.pushToStack(5.0);
        assertDoesNotThrow(() -> command.execute(context,
                Collections.emptyList()));
    }

    @Test
    void squareRootNegative() {
        Exception e = assertThrows(StackException.class,
                () -> command.execute(context, null));
        assertEquals("Not enough arguments in stack", e.getMessage());

        e = assertThrows(InstructionException.class, () -> command.execute(context,
                Arrays.asList(String.valueOf(4.2), String.valueOf(4.2))));
        assertEquals("Wrong usage of SQRT", e.getMessage());

        e = assertThrows(InstructionException.class,
                () -> command.execute(null, null));
        assertEquals("No context", e.getMessage());

        context.pushToStack(-1.0);
        assertThrows(InstructionArgumentException.class,
                () -> command.execute(context, null));
    }
}