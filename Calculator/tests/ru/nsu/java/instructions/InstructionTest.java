package ru.nsu.java.instructions;

import org.junit.jupiter.api.BeforeEach;
import ru.nsu.java.ExecutionContext;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public abstract class InstructionTest {
    protected ExecutionContext context;
    protected ArrayList<String> args;

    @BeforeEach
    public void initialize() {
        args.clear();
        context.clear();
    }


    public InstructionTest() throws IOException {
        context = new ExecutionContext(new FileWriter("testOutput.txt"));
        args = new ArrayList<>();
    }
}