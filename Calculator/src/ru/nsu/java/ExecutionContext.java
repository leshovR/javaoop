package ru.nsu.java;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedList;


public class ExecutionContext {


    private final HashMap<String, Double> defineTable = new HashMap<>();
    private final LinkedList<Double> stack = new LinkedList<>();
    private final Writer out;

    public ExecutionContext(Writer writer) {
        out = writer;
    }

    public void write(String line) throws IOException {
        out.write(line);
        out.flush();
    }

    public void pushToStack(Double value) {
        stack.addFirst(value);
    }

    public Double popFromStack() {
        return stack.removeFirst();
    }

    public int stackSize() {
        return stack.size();
    }

    public void addDefinition(String key, Double value) {
        defineTable.put(key, value);
    }

    public Double getDefinition(String key) {
        return defineTable.getOrDefault(key, null);
    }

    public void clear() {
        stack.clear();
        defineTable.clear();
    }

    public double topOfStack() {
        return stack.getFirst();
    }
}
