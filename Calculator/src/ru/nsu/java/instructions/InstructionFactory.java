package ru.nsu.java.instructions;

import ru.nsu.java.exceptions.FactoryException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

public class InstructionFactory {
    private static final Logger log =
            Logger.getLogger(InstructionFactory.class.getName());
    private final Map<String, Instruction> instructionMap = new HashMap<>();

    public InstructionFactory(String propFile)
            throws IOException, FactoryException {
        log.info("Initializing factory from " + propFile);
        Properties properties = new Properties();
        properties.load(InstructionFactory.class.getResourceAsStream(propFile));
        for (String className : properties.stringPropertyNames()) {
            Instruction instruction;
            try {
                Class c = Class.forName(properties.getProperty(className));
                instruction = (Instruction)c.newInstance();
                instructionMap.put(className, instruction);
            } catch (Exception e) {
                log.severe("Failed to create factory");
                throw new FactoryException("Couldn't create factory");
            }

        }
        log.fine("Factory created successfully");
    }

    public Instruction getInstance(String name) {
        log.info("Returning object mapped to the name \"" + name + "\"");
        return instructionMap.getOrDefault(name, null);
    }
}
