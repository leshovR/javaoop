package ru.nsu.java.instructions.controls;

import ru.nsu.java.ExecutionContext;
import ru.nsu.java.exceptions.CalculatorException;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.exceptions.StackException;
import ru.nsu.java.instructions.Instruction;

import java.util.Arrays;
import java.util.List;

public class Pop implements Instruction {
    public void execute(ExecutionContext context, List<String> args)
            throws CalculatorException {
        if (context == null) {
            throw new InstructionException("No context");
        }
        if (context.stackSize() == 0) {
            throw new StackException("Not enough values in the stack");
        }
        if (args == null || args.size() == 0) {
            context.popFromStack();
        } else if (args.size() <= context.stackSize()) {
            for (String arg : args) {
                new Define().execute(context, Arrays.asList(arg,
                        context.popFromStack().toString()));
            }
        } else {
            throw new StackException("Can't pop this much");
        }
    }
}