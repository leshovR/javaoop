package ru.nsu.java.instructions.controls;

import ru.nsu.java.ExecutionContext;
import ru.nsu.java.exceptions.*;
import ru.nsu.java.instructions.Instruction;

import java.io.IOException;
import java.util.List;

public class Print implements Instruction {
    public void execute(ExecutionContext context, List<String> args)
            throws CalculatorException {
        if (context == null) {
            throw new InstructionException("No context");
        }
        if (context.stackSize() == 0) {
            throw new StackException("Nothing to print");
        }

        if (!args.isEmpty()) {
            throw new InstructionException("Wrong usage of PRINT");
        }

        double value = context.topOfStack();
        try {
            context.write(value + "\n");
        } catch(IOException e) {
            throw new InstructionException("Failed to print");
        }
    }
}
