package ru.nsu.java.instructions.controls;

import ru.nsu.java.ExecutionContext;
import ru.nsu.java.Utils;
import ru.nsu.java.exceptions.InstructionArgumentException;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.instructions.Instruction;

import java.util.List;

public class Define implements Instruction {
    public void execute(ExecutionContext context, List<String> args)
            throws InstructionException {
        if (context == null) {
            throw new InstructionException("No context");
        }
        if (args == null || args.size() != 2) {
            throw new InstructionArgumentException("Wrong arguments");
        }

        String first = args.get(0);
        String second = args.get(1);
        Double value;
        if (Utils.isValidName(first)) {
            if ((value = Utils.getDoubleOrNull(second)) != null) {
                context.addDefinition(first, value);
            } else {
                throw new InstructionArgumentException(second + " is not a valid argument");
            }
        } else {
            throw new InstructionArgumentException(first + " is not a valid name");
        }
    }
}
