package ru.nsu.java.instructions.controls;

import ru.nsu.java.ExecutionContext;
import ru.nsu.java.Utils;
import ru.nsu.java.exceptions.InstructionArgumentException;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.instructions.Instruction;

import java.util.List;

public class Push implements Instruction {

    public void execute(ExecutionContext context, List<String> args)
            throws InstructionException {
        if (context == null) {
            throw new InstructionException("No context");
        }
        if (args == null || args.isEmpty()) {
            throw new InstructionArgumentException("Not enough arguments");
        }
        for (String arg : args) {
            Double value;
            if (Utils.isValidName(arg)) {
                value = context.getDefinition(arg);
            } else {
                value = Utils.getDoubleOrNull(arg);
            }
            if (value == null) {
                throw new InstructionArgumentException("Bad argument");
            }
            context.pushToStack(value);
        }
    }
}
