package ru.nsu.java.instructions.arithmetic;

import ru.nsu.java.ExecutionContext;
import ru.nsu.java.exceptions.CalculatorException;
import ru.nsu.java.exceptions.InstructionArgumentException;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.exceptions.StackException;
import ru.nsu.java.instructions.Instruction;

import java.util.List;

public class SquareRoot implements Instruction {
    public void execute(ExecutionContext context, List<String> args)
            throws CalculatorException {
        if (context == null) {
            throw new InstructionException("No context");
        }
        if (args != null && !args.isEmpty()) {
            throw new InstructionException("Wrong usage of SQRT");
        }
        if (context.stackSize() < 1) {
            throw new StackException("Not enough arguments in stack");
        }

        Double a = context.popFromStack();
        if (a < 0) {
            throw new InstructionArgumentException(
                    "Can't take sqrt of below-zero value " + a);
        }
        context.pushToStack(Math.sqrt(a));
    }
}