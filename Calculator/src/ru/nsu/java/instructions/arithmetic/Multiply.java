package ru.nsu.java.instructions.arithmetic;

import ru.nsu.java.ExecutionContext;
import ru.nsu.java.exceptions.CalculatorException;
import ru.nsu.java.exceptions.InstructionException;
import ru.nsu.java.exceptions.StackException;
import ru.nsu.java.instructions.Instruction;

import java.util.List;

public class Multiply implements Instruction {
    public void execute(ExecutionContext context, List<String> args)
            throws CalculatorException {
        if (context == null) {
            throw new InstructionException("No context");
        }
        if (args != null && !args.isEmpty()) {
            throw new InstructionException("Wrong usage of MULTIPLY");
        }
        if (context.stackSize() < 2) {
            throw new StackException("Not enough arguments in stack");
        }

        Double a = context.popFromStack();
        Double b = context.popFromStack();
        context.pushToStack(a * b);
    }
}
