package ru.nsu.java.instructions;

import ru.nsu.java.ExecutionContext;
import ru.nsu.java.exceptions.CalculatorException;

import java.util.List;

public interface Instruction {
    void execute(ExecutionContext context, List<String> args)
            throws CalculatorException;
}
