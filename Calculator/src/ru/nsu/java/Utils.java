package ru.nsu.java;

public class Utils {
    public static boolean isValidName(String string) {
        return string.matches("[\\w&&\\D]\\w*");
    }

    public static Double getDoubleOrNull(String string) {
        Double d;
        try {
            d = Double.parseDouble(string);
        } catch(NumberFormatException nfe) {
            d = null;
        }
        return d;
    }
}
