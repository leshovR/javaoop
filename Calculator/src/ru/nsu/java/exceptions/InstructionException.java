package ru.nsu.java.exceptions;


public class InstructionException extends CalculatorException {
    public InstructionException() {
        super();
    }

    public InstructionException(String s) {
        super(s);
    }
}
