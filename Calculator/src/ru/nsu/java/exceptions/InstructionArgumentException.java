package ru.nsu.java.exceptions;

public class InstructionArgumentException
        extends InstructionException {
    public InstructionArgumentException() {
        super();
    }

    public InstructionArgumentException(String s) {
        super(s);
    }
}
