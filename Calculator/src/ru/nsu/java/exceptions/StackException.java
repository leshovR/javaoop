package ru.nsu.java.exceptions;

public class StackException extends CalculatorException {
    public StackException() {
        super();
    }

    public StackException(String s) {
        super(s);
    }
}
