package ru.nsu.java.exceptions;

public class FactoryException extends Exception {
    public FactoryException() {
        super();
    }

    public FactoryException(String msg) {
        super(msg);
    }
}
