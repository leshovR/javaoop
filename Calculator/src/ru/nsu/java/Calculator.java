package ru.nsu.java;

import ru.nsu.java.exceptions.CalculatorException;
import ru.nsu.java.exceptions.FactoryException;
import ru.nsu.java.instructions.InstructionFactory;
import ru.nsu.java.instructions.Instruction;

import java.io.*;
import java.util.*;
import java.util.logging.*;

public class Calculator {
    private final Reader in;
    private final Writer out;
    private final ExecutionContext context;
    private InstructionFactory factory;
    private static final Logger log =
            Logger.getLogger(Calculator.class.getName());

    public Calculator(Reader reader, Writer writer) throws IOException,
            FactoryException {
        log.info("Creating calculator");
        in = reader;
        out = writer;
        context = new ExecutionContext(out);
        factory = new InstructionFactory("factory.properties");
        log.fine("Calculator successfully created");
    }

    public void runProgram() throws IOException {
        log.info("Executing method runProgram()");
        BufferedReader bReader = new BufferedReader(in);
        String line;
        while ((line = bReader.readLine()) != null) {
            if (line.length() == 0) {
                continue;
            }
            try (Scanner scanner = new Scanner(new StringReader(line))) {
                log.info("Executing command " + line);

                String name;
                name = scanner.next();
                Instruction instruction = factory.getInstance(name);
                if (instruction != null) {
                    List<String> args = new ArrayList<>();
                    while (scanner.hasNext()) {
                        args.add(scanner.next());
                    }
                    instruction.execute(context, args);
                    log.fine("Command " + name + " successfully executed");
                } else {
                    log.warning("Couldn't execute unknown command" + name);
                    out.write("Unknown command" + name);
                }
            } catch (CalculatorException e) {
                log.log(Level.WARNING, "Exception trying to execute command " + line, e);
            }
        }
        log.fine("Method runProgram() finished successfully");
    }
}
