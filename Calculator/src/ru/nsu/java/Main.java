package ru.nsu.java;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    private static final Logger log =
            Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        try {
            LogManager.getLogManager()
                    .readConfiguration(Main.class
                            .getResourceAsStream("logging.properties"));
        } catch (IOException e) {
            System.err.println("Couldn't setup loggers");
        }
        log.info("Starting program");
        InputStream in;
        OutputStream out;
        if (args.length == 2) {
            log.info("Opening requested files for input and output");
            try {
                in = new FileInputStream(args[0]);
                log.fine(args[0] + " is opened for reading");
                out = new FileOutputStream(args[1]);
                log.fine(args[1] + " is opened for writing");
            } catch (FileNotFoundException e) {
                log.severe("Couldn't find or open the files");
                return;
            }
        } else {
            in = System.in;
            out = System.out;
        }
        try (Reader reader = new InputStreamReader(in);
             Writer writer = new OutputStreamWriter(out)) {
            Calculator calculator = new Calculator(reader, writer);
            calculator.runProgram();
        } catch (Exception e) {
            log.log(Level.SEVERE, "Exception occurred. Can't continue", e);
        }
        log.fine("Program execution finished");
    }
}
